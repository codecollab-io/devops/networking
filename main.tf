terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

provider "google" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

provider "google-beta" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

resource "google_compute_router" "router_us" {
  region = "us-central1"

  name    = "router-us"
  network = "default"
}

resource "google_compute_router_nat" "router_us_nat" {
  name                               = "router-us-nat"
  router                             = google_compute_router.router_us.name
  region                             = google_compute_router.router_us.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

resource "google_compute_router" "router_eu" {
  region = "europe-west4"

  name    = "router-eu"
  network = "default"
}

resource "google_compute_router_nat" "router_eu_nat" {
  name                               = "router-eu-nat"
  router                             = google_compute_router.router_eu.name
  region                             = google_compute_router.router_eu.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

resource "google_compute_router" "router_sg" {
  region = "asia-southeast1"

  name    = "router-sg"
  network = "default"
}

resource "google_compute_router_nat" "router_sg_nat" {
  name                               = "router-sg-nat"
  router                             = google_compute_router.router_sg.name
  region                             = google_compute_router.router_sg.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}
